# loker

Berikut penjelasan secara mendetail mengenai program loker 
1.bagaimana cara menjalankan programnya?
-pertama tama install dahulu golang, setelah berhasil menginstall jgn lupa definisikan system environment variable nya
contoh :  GOPATH C:\{folder yang ada di komputer kalian}
-lalu clone/download program loker, taruh di folder yg sudah anda definisikan system environment variable C:\{folder yang ada di komputer kalian}
-menjalankan program dari command line, buka command line anda lalu masukan perintah 
`C:\{folder yang ada di komputer kalian}go run loker.go`

2. penjelasan secara detail program nya
di program ini mengenal perintah masukan yaitu : 
- init {definisi jumlah loker} => disini adalah definis awal banyak nya jumlah loker
- input {tipe identitas} {nomor identitas} => disini adalah proses memasukkan tipe identitas dan nomor identitas ke loker
- status => untuk melihat data nomor identitas apa saja yg sudah ada pada loker
- leave {nomor loker} => disini adalah proses mengosongkan sebuah loker
- find {nomor identitas} => disini adalah proses mencari data berdasarkan nomor identitas dan akan menampilkan di loker mana nomor identitas tersebut dimasukkan
- search {tipe identitas} => disini adalah proses pencarian tipe identitas yang mana dari tipe identitas tsb apa saja nomor identitas didalamnya
- exit => proses mengakhiri program

di dalam file loker.go terdapat kepingan code dmn disini saya akan menjelaskan tahap demi tahapanny:

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"strconv"
)

kepingan code diatas adalah library yg dibutuhkan:
-bufio,os => untuk input & output program
-fmt => untuk cetak/print 
-strings => mendefinisikan bahwa string/tulisan
-strconv => u/ konversi string ke integer

------------------------------------------------------
type loker struct {
	tipe_identitas string
	nomor_identitas string
}

var a []loker
var b []int

func RemoveIndex(s []int, index int) []int {
    return append(s[:index], s[index+1:]...)
}

kepingan kode diatas adalah menjelaskan bahwa pertama kita harus mendefinisikan loker struktur yaitu terdiri dari tipe identitas & nomor identitas
berikutnya kita definisikan array untuk loker, array bantuan u/ mencek bahwa loker sudah penuh atau belum disini(terhubung dengan RemoveIndex),

---------------------------------------------------
func main() {}

code disini adalah code utama dari program yang akan dijalankan

----------------------------------------------------
if strings.Contains(text, "init") {
	words := strings.Fields(text)
	if(len(words) > 1){
		var indeks_val int
		indeks_val = len(words) - 1
		// string to int
	    jml_Loker, err := strconv.Atoi(words[indeks_val])
	    if err != nil {
	        // ... handle error
	        panic(err)
	    }
		a = make([]loker,jml_Loker)
		fmt.Println("Berhasil membuat loker dengan jumlah ",words[indeks_val])
	}
	
}

penjelasan disini adalah perintah masukan untuk mendefinisikan berapa banyak jumlah loker yang akan tersedia

--------------------------------------------------------------------
if strings.Contains(text, "input"){
	words := strings.Fields(text)
	if(len(words) > 1 && len(words) < 4){
		var indeks_val_2, indeks_val_1 int
		indeks_val_2 = len(words) - 1
		indeks_val_1 = indeks_val_2 - 1

		data := loker{tipe_identitas: words[indeks_val_1],nomor_identitas: words[indeks_val_2]}

		if(len(b) == len(a)){
			fmt.Println("Maaf loker sudah penuh")
		}
		
		for j := 0; j < len(a); j++ {
			// input proses
			if(a[j] == lk){
				
				a[j] = data
				b = append(b, 1)
				fmt.Println("Kartu identitas tersimpan di loker nomor ",j+1)
				break
			} else {
				continue
			}

		}

	}else{
		fmt.Println("Perintah tidak ditemukan")
	}
}
penjelasan code disini adalah proses input/memasukkan tipe identitas dan nomor identitas ke dalam loker

--------------------------------------------------------------------------------------------
if(text == "status"){
	fmt.Println("No Loker","Tipe Identitas","No Identitas")
	for k := 0; k < len(a); k++ {
		fmt.Println(k+1,"		",a[k].tipe_identitas,"		",a[k].nomor_identitas)
		
	}
}
penjelasan code disini adalah untuk mencek tipe identitas apa dan nomor identitas apa yang sudah masuk dalam loker

-------------------------------------------------------------------------------------------
if strings.Contains(text, "leave"){
	words := strings.Fields(text)
	if(len(words) > 1 && len(words) < 3){
		var indeks_val_noloker int
		indeks_val_noloker = len(words) - 1

		no_loker, err := strconv.Atoi(words[indeks_val_noloker])
	    if err != nil {
	        // ... handle error
	        panic(err)
	    }
		for j := 0; j < len(a); j++ {
			if(no_loker == j+1){
				a[j] = lk
				b = RemoveIndex(b, j)
				break
			}else{
				continue
			}
			
		}
		fmt.Println("Loker nomor ",no_loker," berhasil dikosongkan")
	}
}		
penjelasan code disini adalah proses mengosongkan loker sesuai nomor loker yang akan dikosongkan

--------------------------------------------------------------------------
if strings.Contains(text, "find"){
	words := strings.Fields(text)
	if(len(words) > 1 && len(words) < 3){
		var indeks_val_noidentitas int
		indeks_val_noidentitas = len(words) - 1

		for j := 0; j < len(a); j++ {
			if(a[j].nomor_identitas == words[indeks_val_noidentitas]){
				fmt.Println("Kartu identitas tersimpan di loker nomor ",j+1)
			}
			
		}
		
	}
}
penjelasan code disini adalah proses pencarian nomor identitas yang mana akan menjelaskan tempat nomor loker dari nomor identitas yg akan dicari

----------------------------------------------------------------------------
if strings.Contains(text, "search"){
	words := strings.Fields(text)
	if(len(words) > 1 && len(words) < 3){
		var indeks_val_tipeidentitas int
		indeks_val_tipeidentitas = len(words) - 1

		var list_no_identitas []string
		for j := 0; j < len(a); j++ {
			if(a[j].tipe_identitas == words[indeks_val_tipeidentitas]){
				list_no_identitas = append(list_no_identitas, a[j].nomor_identitas)
			}
			
		}
		String_no_identitas := strings.Join(list_no_identitas,",")
		fmt.Println(String_no_identitas)
	}
}
penjelasan dari code ini adalah proses pencaria dari tipe identitas, yg mana akan menampilkan nomor identitas apa saja yg ada pada tipe identitas yg dicari

----------------------------------------------------------------------------------
if(text == "exit") {
	// exit if user entered an empty string
	break
}
penjelasan code disini adalah sebuah perintah untuk mengakhiri dari sebuah program

----------------------------------------------------------------------------------

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/nando6/loker.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/nando6/loker/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
