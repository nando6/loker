package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"strconv"
)

type loker struct {
	tipe_identitas string
	nomor_identitas string
}

var a []loker
var b []int

func RemoveIndex(s []int, index int) []int {
    return append(s[:index], s[index+1:]...)
}

func main() {
	var lk loker
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("")
		// reads user input until \n by default
		scanner.Scan()
		// Holds the string that was scanned
		text := scanner.Text()
		if strings.Contains(text, "init") {
			words := strings.Fields(text)
			if(len(words) > 1){
				var indeks_val int
				indeks_val = len(words) - 1
				// string to int
			    jml_Loker, err := strconv.Atoi(words[indeks_val])
			    if err != nil {
			        // ... handle error
			        panic(err)
			    }
				a = make([]loker,jml_Loker)
				fmt.Println("Berhasil membuat loker dengan jumlah ",words[indeks_val])
			}
			
		} else if strings.Contains(text, "input"){
			words := strings.Fields(text)
			if(len(words) > 1 && len(words) < 4){
				var indeks_val_2, indeks_val_1 int
				indeks_val_2 = len(words) - 1
				indeks_val_1 = indeks_val_2 - 1

				data := loker{tipe_identitas: words[indeks_val_1],nomor_identitas: words[indeks_val_2]}

				if(len(b) == len(a)){
					fmt.Println("Maaf loker sudah penuh")
				}
				
				for j := 0; j < len(a); j++ {
					// input proses
					if(a[j] == lk){
						
						a[j] = data
						b = append(b, 1)
						fmt.Println("Kartu identitas tersimpan di loker nomor ",j+1)
						break
					} else {
						continue
					}

				}

			}else{
				fmt.Println("Perintah tidak ditemukan")
			}
		} else if(text == "status"){
			fmt.Println("No Loker","Tipe Identitas","No Identitas")
			for k := 0; k < len(a); k++ {
				fmt.Println(k+1,"		",a[k].tipe_identitas,"		",a[k].nomor_identitas)
				
			}
		} else if strings.Contains(text, "leave"){
			words := strings.Fields(text)
			if(len(words) > 1 && len(words) < 3){
				var indeks_val_noloker int
				indeks_val_noloker = len(words) - 1

				no_loker, err := strconv.Atoi(words[indeks_val_noloker])
			    if err != nil {
			        // ... handle error
			        panic(err)
			    }
				for j := 0; j < len(a); j++ {
					if(no_loker == j+1){
						a[j] = lk
						b = RemoveIndex(b, j)
						break
					}else{
						continue
					}
					
				}
				fmt.Println("Loker nomor ",no_loker," berhasil dikosongkan")
			}
		} else if strings.Contains(text, "find"){
			words := strings.Fields(text)
			if(len(words) > 1 && len(words) < 3){
				var indeks_val_noidentitas int
				indeks_val_noidentitas = len(words) - 1

				for j := 0; j < len(a); j++ {
					if(a[j].nomor_identitas == words[indeks_val_noidentitas]){
						fmt.Println("Kartu identitas tersimpan di loker nomor ",j+1)
					}
					
				}
				
			}
		} else if strings.Contains(text, "search"){
			words := strings.Fields(text)
			if(len(words) > 1 && len(words) < 3){
				var indeks_val_tipeidentitas int
				indeks_val_tipeidentitas = len(words) - 1

				var list_no_identitas []string
				for j := 0; j < len(a); j++ {
					if(a[j].tipe_identitas == words[indeks_val_tipeidentitas]){
						list_no_identitas = append(list_no_identitas, a[j].nomor_identitas)
					}
					
				}
				String_no_identitas := strings.Join(list_no_identitas,",")
				fmt.Println(String_no_identitas)
			}
		} else if(text == "exit") {
			// exit if user entered an empty string
			break
		}

	}

	// handle error
	if scanner.Err() != nil {
		fmt.Println("Error: ", scanner.Err())
	}
}